#include<stdio.h>


struct student {
	char fname[20];
	char subject[20];
	int marks;
};

int main(){
 
	struct student s[5];
	
	printf("Enter student details\n");
	for(int i=0;i<5;i++){
	
		printf("Enter the first name of the student: ");
		scanf("%s",&s[i].fname);
		printf("Enter the subject: ");
		scanf("%s",&s[i].subject);
		printf("Enter the marks: ");
		scanf("%d",&s[i].marks);
		printf("\n");
	}
	
	printf("\nStudent Information\n");
	printf("\n");

	for(int i=0;i<5;i++){
		
		printf("Name: %s \n",s[i].fname);
		printf("Subject: %s \n",s[i].subject);
		printf("Marks: %d \n",s[i].marks);
		printf("\n");
	}
   return 0;
}
